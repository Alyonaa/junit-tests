import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class JunitTests {
    CommandCenter commandCenter = new CommandCenter();
    ArrayList<Spaceship> overallShipsTestList = new ArrayList<>();
    ArrayList<Spaceship> expectedCargoSpaceTestList = new ArrayList<>();
    ArrayList<Spaceship> expectedNullTestList = new ArrayList<>();
    ArrayList<Spaceship> actualList = new ArrayList<>();
    ArrayList<Spaceship> noPowerAndCargoTestList = new ArrayList<>();
    Spaceship expectedPowerTest;
    Spaceship actual;
    String expectedStringNullTest;
    String expectedStringTest;


    @BeforeEach
    public void setUp() {
        expectedPowerTest = new Spaceship("A", 100, 50, 15);
        overallShipsTestList.add(expectedPowerTest);
        overallShipsTestList.add( new Spaceship("L", 100, 15, 2002));
        overallShipsTestList.add( new Spaceship("Y", 15, 1, 2002));
        noPowerAndCargoTestList.add(new Spaceship("O", 0, 0, 15));
        noPowerAndCargoTestList.add(new Spaceship("N", 0, 0, 15));
        noPowerAndCargoTestList.add(new Spaceship("A", 0, 15, 15));
        expectedCargoSpaceTestList.add(expectedPowerTest);
    }

    @AfterEach
    public void after() {
        overallShipsTestList.clear();
        expectedCargoSpaceTestList.clear();
        noPowerAndCargoTestList.clear();
        actualList.clear();
    }

    @Test
    public void getMostPowerfulShip_mostPowerfulShipExists_returnsFirst() {
        actual = commandCenter.getMostPowerfulShip(overallShipsTestList);
        Assertions.assertEquals(expectedPowerTest, actual);
    }

    @Test
    public void getMostPowerfulShip_returnsFirst() {
        actual = commandCenter.getMostPowerfulShip(overallShipsTestList);
        Assertions.assertEquals(expectedPowerTest, actual);
    }


    @Test
    public void getMostPowerfulNone() {
        actual = commandCenter.getMostPowerfulShip(noPowerAndCargoTestList);
        Assertions.assertNull(actual);
    }

    @Test
    public void getAllShipsWithEnoughCargoSpaceTest() {
        actualList = commandCenter.getAllShipsWithEnoughCargoSpace(overallShipsTestList, 40);
        Assertions.assertEquals(expectedCargoSpaceTestList, actualList);
    }

    @Test
    public void getAllShipsWithEnoughCargoSpaceNone() {
        actualList = commandCenter.getAllShipsWithEnoughCargoSpace(overallShipsTestList, 1000);
        Assertions.assertEquals(expectedNullTestList, actualList);
    }

    @Test
    public void getShipByNameTest() {
        expectedStringTest = expectedPowerTest.getName();
        Spaceship actual = commandCenter.getShipByName(overallShipsTestList, expectedStringTest);
        Assertions.assertEquals(expectedStringTest, actual.getName());
    }

    @Test
    public void getShipByNameTestNone() {
        expectedStringNullTest = "Naumova";
        Assertions.assertNull(commandCenter.getShipByName(overallShipsTestList, expectedStringNullTest));
    }

    @Test
    public void getCivilianShipsTest() {
        actualList = commandCenter.getAllCivilianShips(noPowerAndCargoTestList);
        Assertions.assertEquals(noPowerAndCargoTestList, actualList);
    }

    @Test
    public void getCivilianShipsTestNone() {
        actualList = commandCenter.getAllCivilianShips(overallShipsTestList);
        Assertions.assertEquals(expectedNullTestList, actualList);
    }
}