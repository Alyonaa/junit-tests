package itis.quiz.spaceships;


import java.util.ArrayList;

    // Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
    public class CommandCenter implements itis.quiz.spaceships.SpaceshipFleetManager {
        ArrayList<Spaceship> ships = new ArrayList<>();

        @Override
        public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
            int strong = 0;
            int answer = -1;
            for (int i = 0; i < ships.size(); i++) {
                if (ships.get(i).getFirePower() > strong) {
                    strong = ships.get(i).getFirePower();
                    answer = i;
                }
            }
            if (answer != -1) {
                return ships.get(answer);
            }
            return null;
        }

        @Override
        public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
            for (Spaceship ship : ships) {
                if (ship.getName().equals(name)) {
                    return ship;
                }
            }
            return null;
        }

        @Override
        public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ship, Integer cargoSize) {
            ArrayList<Spaceship> res = new ArrayList<>();
            for (Spaceship spaceship : ship) {
                if (spaceship.getCargoSpace() >= cargoSize) {
                    res.add(spaceship);
                }
            }
            return res;
        }

        @Override
        public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
            ArrayList<Spaceship> shipB = new ArrayList<>();
            for (Spaceship ship : ships) {
                if (ship.getFirePower() == 0) {
                    shipB.add(ship);
                }
            }
            return shipB;
        }
    }

