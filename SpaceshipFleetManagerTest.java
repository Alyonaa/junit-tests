import itis.quiz.spaceships.CommandCenter;
import itis.quiz.spaceships.Spaceship;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    CommandCenter commandCenter = new CommandCenter();

    public SpaceshipFleetManagerTest(CommandCenter commandCenter) {
    }

    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        System.out.println("Passed tests: " + getScore());
    }

    private static int getScore(){
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        int score = 0;
        if(spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnsFirst()){
            score++;
        }
        if(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpaceNone()){
            score++;
        }
        if(spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpaceTest()){
            score++;
        }
        if(spaceshipFleetManagerTest.getCivilianShipsTest()){
            score++;
        }
        if(spaceshipFleetManagerTest.getCivilianShipsTestNone()){
            score++;
        }
        if(spaceshipFleetManagerTest.getMostPowerfulNone()){
            score++;
        }
        if(spaceshipFleetManagerTest.getMostPowerfulShip_returnsFirst()){
            score++;
        }
        if(spaceshipFleetManagerTest.getShipByNameTest()){
            score++;
        }
        if(spaceshipFleetManagerTest.getShipByNameTestNone()){
            score++;
        }
        return score;
    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnsFirst() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 0, 5));
        testShipsList.add(new Spaceship("Doo", 100, 0, 5));
        Spaceship result = commandCenter.getMostPowerfulShip(testShipsList);
        if (result.getFirePower() == 100 && result.getName().equals("Foo")) {
            testShipsList.clear();
            return true;
        }
        testShipsList.clear();
        return false;
    }


    private boolean getMostPowerfulShip_returnsFirst() {
        ArrayList<Spaceship> testShipList = new ArrayList<>();
        testShipList.add(new Spaceship("Foo", 100, 0, 3));
        testShipList.add(new Spaceship("Doo", 10, 0, 3));
        Spaceship result = commandCenter.getMostPowerfulShip(testShipList);
        return result.getFirePower() == 100 && result.getName().equals("Foo");

    }
    private boolean getMostPowerfulNone(){
        ArrayList<Spaceship> testShipList = new ArrayList<>();
        testShipList.add(new Spaceship("Foo", 0, 0, 3));
        testShipList.add(new Spaceship("Doo", 0, 0, 3));
        testShipList.add(new Spaceship("Boo", 0, 11, 3));
        Spaceship result = commandCenter.getMostPowerfulShip(testShipList);
        return result == null;
    }

    private boolean getAllShipsWithEnoughCargoSpaceTest() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 10, 11, 3));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 40);
        for (Spaceship spaceship : testShipsList) {
            return spaceship.getCargoSpace() == 50;
        }
        return false;
    }

    private boolean getAllShipsWithEnoughCargoSpaceNone() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 100, 39, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 10, 11, 3));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testShipsList, 40);
        return result.isEmpty();
    }

    private boolean getShipByNameTest() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        String s = "Foo
        ";
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 10, 11, 3));
        Spaceship result = commandCenter.getShipByName(testShipsList, s);
        return result.getName().equals(s);
    }

    private boolean getShipByNameTestNone() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        String s = "Joo";
        testShipsList.add(new Spaceship("Foo", 100, 50, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 10, 11, 3));
        Spaceship result = commandCenter.getShipByName(testShipsList, s);
        return result == null;
    }

    private boolean getCivilianShipsTest() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 0, 50, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 100, 11, 3));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        for (Spaceship ships : result) {
            return ships.getFirePower() == 0 && ships.getName().equals("Foo");
        }
        return false;
    }
    private boolean getCivilianShipsTestNone() {
        ArrayList<Spaceship> testShipsList = new ArrayList<>();
        testShipsList.add(new Spaceship("Foo", 10, 50, 3));
        testShipsList.add(new Spaceship("Doo", 10, 25, 3));
        testShipsList.add(new Spaceship("Boo", 100, 11, 3));
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testShipsList);
        return result.isEmpty();
    }